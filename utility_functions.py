"""
Created on Fri Mar 12, 2021 18:20:00

@author: Hannes Kohlmann
@company: OVE Service GmbH 
@website: https://www.aldis.at
ALDIS - Austrian Lightning Detection Information System
"""

import numpy as np
import scipy
from scipy import interpolate
from scipy.ndimage import rotate
import csv
import math as m
import matplotlib.pyplot as plt
from PIL import Image
if __name__ != "__main__":
    import meep as mp

t_ord_of_magn = {"s":1,
               "ms": 1e-3,
               "mus":1e-6,
               "ns":1e-9,
               }

class RS_Model:
    def __init__(self, src_configs, tower=None, discontinuity=False, t_ord_of_magnitude=1e-6, a=1000,resolution=250):
        """
        i_params is a list of dicts, which all hold Heidler function parameters: 
        {"i":1, "tau1":1.8, "tau2":95,"n":2,"t_ord_of_magn":"mus"}
        mp scaling is the Class the mtlm lambda must come in meters!
        Then by means of the mp_units class it is easy to calculate the mp_unit lambda
        
        src_configs is structured this way:
        src_configs = {"src_x": src_x,
           "surface_z": surface_z,
           "v_rs": v_rs,
           "sources_per_unit": sources_per_unit, 
           "stroke_length": stroke_length,
           "source_component":mp.Ez,
           }
        """
        self.mp_units = MeepUnits(a=1000, resolution=resolution)
        self.i_params = src_configs["channel_base_current"]
        if type(self.i_params) is str:
            self.current_type = "real"
            self.filename = self.i_params
            self.readFile()
        else:
           self.current_type = "analytical"
           self.filename = None

        self.t_ord_of_magnitude = t_ord_of_magnitude
        self.src_configs = src_configs
        self.mtlm = self.src_configs["mtlm"]
        self.mtlm["lambda"] = self.mtlm["lambda"]/self.mp_units.a
        self.tower = tower
        self.discontinuity = discontinuity 
            
    def readFile(self):
        time = []
        amplitude = []
        with open(self.filename, "r") as f:
            data = csv.reader(f, delimiter=',')
            for row in data:
                time.append(float(row[0]))
                amplitude.append(float(row[1]))
        f.close()

        time = np.array(time)
        amplitude = np.array(np.multiply((-1),amplitude))
        # Interpolation function of real current:
        self._current_interp = interpolate.interp1d(time, amplitude, kind='cubic')
    def set_mtlm(self, mode, **kwargs):
        """
        mode must be true or false
        lam must be in meters and mtlm_type must be "lin" or "exp"
        """
        if mode == False:
            self.mtlm["lambda"] = 0
            return
        
        self.mtlm["type"] = kwargs["type"]
        self.mltm["lambda"] = kwargs["lambda"]/self.mp_units.a
    def set_i_params(self, i_params):
        self.i_params = i_params
    
    def stroke(self):
        print("Subs. stroke (SU)\n: " + str(self.i_params))
        
    def mtlm_factor(self, height):
        """
        height must come in MEEP units
        """
        if self.mtlm == {}:
            return 1
        
        if self.mtlm["lambda"]==0:
            return 1
        
        if self.mtlm["type"] == "exp":
                return m.exp(-height/self.mtlm["lambda"])
        elif self.mtlm["type"] == "lin":
            fact = (self.mtlm["lambda"]-height)/self.mtlm["lambda"]
            if fact>0:
                return fact
            else:
                return 0
        
    def _current_fct(self, height, t_shift, t_shift_heaviside=0, channel=False, wall_src=False,):
        """
        As t itself, shift comes in mp unit and must be converted to the correct base 
        (mus, ms, ns,..)
        t_shift_heaviside is an extra time retardation function to check (t<t_shift_heaviside) when the source may activate (e.g. if two step functions corcur)
        channel = True --> actual channel
        channel = False --> current is injected from the tower and doesn't get attenuated!
        """
        def wrapper(t):
            eta = []
            t_i = [] # this is just a variable substitution used for the function
            current_function = 0
            t_shift_t = self.mp_units.mpt_to_t(t_shift, self.t_ord_of_magnitude)
            t_orig = self.mp_units.mpt_to_t(t, self.t_ord_of_magnitude)
            t = t_orig-t_shift_t
            t_shift_h = self.mp_units.mpt_to_t(t_shift_heaviside, self.t_ord_of_magnitude)
           
            # return values right away before even checking which type of function, if time certain criteria are satisfied
            if t<0:  return 0
            if t<t_shift_h-t_shift_t:  return 0
            
            # Set mtlm_factor (can also be checked in advance, since it is applicable to every model!)
            mtlm_factor = 1
            if channel:
                mtlm_factor = self.mtlm_factor(height)
            if wall_src:
                mtlm_factor = 1
            
            if self.current_type == "analytical":
                for idx, i_params in enumerate(self.i_params):
                    eta.append(m.exp(-i_params["tau1"]/i_params["tau2"]*(i_params["n"] \
                                     *i_params["tau2"]/i_params["tau1"])**(1/i_params["n"])))
                    t_i.append((t/i_params["tau1"])**i_params["n"])
                    current_function += i_params["i"]/eta[idx]*t_i[idx]/(t_i[idx]+1) * m.exp(-t/i_params["tau2"])
                
                return mtlm_factor*current_function
                
            elif self.current_type=="real":
                return mtlm_factor*self._current_interp(t)
        return wrapper
    
    def sources(self, point_src=False, wall_src=False):
        """
        This function builds the source array that the MEEP simulation can readily use in the Sim(...) instanciation
        and can be called like RS_Model(...).sources()
        
        src_configs is structured this way:
        src_configs = {"src_x": src_x,
               "src_z": src_z,
               "v_rs": v_rs,
               "sources_per_unit": sources_per_unit, 
               "stroke_length": stroke_length,
               "source_component":mp.Ez,
               }
        
        This function does also all the work to include the tower model source currents
        """
        sc = self.src_configs
        sources = []
        v_rs = sc["v_rs"]

        sources_per_unit = sc["sources_per_unit"]
        source_extent = 1/sources_per_unit
        src_offset=source_extent/2#source_extent/2 # shifts the full source array up by source_extent/2
        stroke_length = sc["stroke_length"]
        source_component = sc["source_component"]
        
        src_x = sc["src_x"]
        surface_z = sc["surface_z"]+src_offset
        
        if self.tower != None:
            h_tower = self.tower["H"]
            rho_t = self.tower["rho_t"]
            rho_g = self.tower["rho_g"]
            src_z = surface_z + h_tower
        else:
            h_tower = 0
            src_z = surface_z
            
        total_nr_src = int(stroke_length*sources_per_unit)
        if point_src: # in case that just a point source shall be active for testing
            total_nr_src = 1
        total_nr_src_tower = int(h_tower*sources_per_unit) # height error of max source_extent, if truncated
        # ************************************************************************
        # *************      RS ENGINEERING MODEL, no tower    *******************
        # ************************************************************************
        for i in range(total_nr_src):
            h = i*source_extent # z in m --> i/nr_src_per_unit*1000m
            start_time = h/(v_rs/self.mp_units.c0)
            if wall_src:
                start_time = 0
            factor = 1
            sources.append(mp.Source(mp.CustomSource(self._current_fct(height=h, 
                                                                      t_shift=start_time, 
                                                                      channel=True,
                                                                      wall_src=wall_src),
                                                     start_time = start_time, # before it is 0!
                                                     end_time = 100),
                                    amplitude = factor, #amplitude scaling
                                    component=source_component,
                                    center=mp.Vector3(src_x,0,src_z+h), 
                                    size=mp.Vector3(0,0,source_extent)))
  
        return sources
        # ************************************************************************

        
    def plot_current(self, t_len, t_res, label, t_shift=0, factor=1, normalize = False, figure="new", ax=None):
        """
        t_len in mus (microseconds) will be converted to meep units
        """

        t = [i*t_res  for i in range(int(t_len/t_res))]
        mp_t = list(map(lambda t: self.mp_units.t_to_mpt(t,1e-6), t))
        
        
        current = list(map(self._current_fct(height=0,t_shift=t_shift), mp_t)) # nice, this works now pretty nicely because the wrapper gives me back a function ;)
        curr_wrapper = interpolate.interp1d(mp_t,current, kind='cubic')
        
        if figure=="new":
            plt.figure()
            ax = plt.gca()
        elif figure=="same":
            ax = ax
            
        ax.plot(t, current/np.max(current) if normalize==True else current, label=label)
        #plt.plot(t, interp_curr(t), label="interpolated")
        #plt.plot(t, t_res*np.cumsum(current))

        ax.set_xlabel("Time [μs]")
        ax.set_ylabel("Normalized current [kA]" if normalize==True else "Current [kA]")
        ax.set_xlim(0, t_len)
        ax.grid()
        plt.show()
            
        return ax

    def plot_current_fft(self, t_len, t_res, label, from_fq, to_fq, dB=False, figure="new", ax=None):
        
        t = [i*t_res  for i in range(int(t_len/t_res))]
        mp_t = list(map(lambda t: self.mp_units.t_to_mpt(t,1e-6), t))
        
        
        current = list(map(self._current_fct(height=0,t_shift=0), mp_t)) # nice, this works now pretty nicely because the wrapper gives me back a function ;)

        yf = scipy.fft.fft(current)
        yf = yf
        print(t[1]-t[0])
        freqs = scipy.fft.fftfreq(len(current),(t[1]-t[0])*1e-6)
        #xf = scipy.fft.fftshift(freqs)
        N = len(freqs)
        
        if dB:
            log_yf = 20*np.log10(np.abs(2.0/N*yf[0:N//2]))
        else:
            log_yf =np.abs(2.0/N*yf[0:N//2])

        log_yf -= log_yf[2]
        if figure=="new":
            plt.figure()
            ax = plt.gca()
        elif figure=="same":
            ax = ax
        if dB:
            ax.semilogx(freqs[0:len(log_yf)]/1000, log_yf, label=label)
            ax.set_xlabel("f [kHz]")
            ax.set_ylabel(r"Normalized current [dB]")
        else:
            ax.loglog(freqs[0:len(log_yf)]/1000, log_yf, label=label)
            ax.set_xlabel("f [kHz]")
            ax.set_ylabel(r"Current [kA]")
        ax.set_xlim(from_fq/1e3, to_fq/1e3)
        ax.set_ylim(-100, 5)
        ax.legend()
        ax.grid()
        return ax
      
class MeepUnits:
    """
    This class contains the configs and methods to convert the units 
    from MEEP to dimensionful units, vice versa
    """
    def __init__(self, a, resolution, t_order_of_magnitude=1e-6):
        self.c0 = 299792458
        self.a = a # m 
        self.resolution = resolution
        self.t_ord_of_magn = t_order_of_magnitude
        
    def mp_scale_factor(self, t_order_of_magnitude):
        """
        returns mp scale (e.g. 3,3 mus)
        
        ord_of_magn must be 1e-3 if ms, 1e-6 if μs, etc
        """
        return self.a/self.c0/t_order_of_magnitude #converts to "..s" and returns value

    def mpt_to_t(self, mpt, t_order_of_magnitude):
        return mpt*self.mp_scale_factor(t_order_of_magnitude)
    
    def t_to_mpt(self, t, t_order_of_magnitude):
        return t/self.mp_scale_factor(t_order_of_magnitude)
    
# ********************************************************************
class TerrainModel():
    def __init__(self, kind, cell_x, resolution, params=None, filename="", block_center_thickness_size=[]):
        """
        kind: 'file', 'cos', 'sin' 
        ... should build the terrain model correspondingly
        cell_x: extent of the cell in meep units!
        
        params is a dictionary for example if kind is 'cos_top', 
        then params contains keys 'peak_to_peak' and 'periodicity'
        """
        self.mp_units = MeepUnits(a=1000, resolution=resolution)
        self.cell_x = cell_x*self.mp_units.a # cell extent in radial direction in meters!
        
        self.distance = []
        self.height = []
        self.min = float(0)
        self.max = float(0)
        self._terrain_fct = None
        self.filename = filename
        self.params = params
        self.block_center_thickness_size = block_center_thickness_size
        
        if kind=="file":
            self.readFile() # read data from file and generate cubic interpolation function
        elif kind == "cos_floor":
            self._terrain_fct = self.harmonic(params["peak_to_peak"], 
                                              params["periodicity"], 
                                              angle=m.pi)
        elif kind == "cos_top":
            self._terrain_fct = self.harmonic(params["peak_to_peak"], 
                                              params["periodicity"], 
                                              angle=0)
        elif kind == "testobject":
            self._terrain_fct = self.testobject()
        
        # The next fcts include evaluations of the functions which were 
        #initialized for the SimulationEnvironment
        self.minmax()
    
    def testobject(self):
        surface_at_origin = 0.5
        interpolation_step = 0.0001
        self.dist_vec = np.r_[0:5:interpolation_step]
        self.height_vec = np.ones(np.shape(self.dist_vec))*surface_at_origin
        
        for b in self.block_center_thickness_size:
            sizes = b[2:]
            for i, c in enumerate(sizes):
                for idx, j in enumerate(self.dist_vec):
                    if j>=b[0]-c/2 and j<=b[0]+c/2:
                        self.height_vec[idx] = surface_at_origin + (i+1)*b[1]
        plt.plot(self.dist_vec, np.array(self.height_vec)-0.5)
        return interpolate.interp1d(self.dist_vec, self.height_vec, kind='cubic')

    def harmonic(self,peak_to_peak,periodicity,angle):
        
        """
        peak_to_peak in mp_units a
        periodicity in mp_units a
        angle in radians!
        """
        return (lambda x: peak_to_peak/2*(1+m.cos(2*m.pi*x/periodicity+angle)))
        
    def readFile(self):
        # distance = []
        # height = []
        # h_extent = []
        # with open(self.filename, "r") as f:
        #     data = csv.reader(f, delimiter=',')
        #     for row in data:
        #         distance.append(float(row[0]))
        #         height.append(float(row[1]))
        # f.close()
        
        self.terrain_data = np.genfromtxt(self.filename, delimiter=",")/self.mp_units.a
        self.distance = self.terrain_data[:,0]        
        self.height = self.terrain_data[:,1]
        self._terrain_fct = interpolate.interp1d(self.distance, self.height, kind='cubic')
            
    def minmax(self):
        x = np.array([i for i in range(int(self.cell_x))])/self.mp_units.a
        y = list(map(lambda x_: self._terrain_fct(x_), x))
        self.min = min(y)
        self.max = max(y)
    
    def terrain_fct(self, x):
        """
        makes the terrain_fct more obvious for the outside
        """
        return self._terrain_fct(x)

    def prism_func(self, geom_surface_point, cell_x, material, onevertex=False):
        surface_offset = 0#geom_surface_point
        gb_nd = self.terrain_data
        #gb_nd = np.vstack([[], ])
        print("THis is the terrain data:", gb_nd)
        
        geometry = []
        extent = cell_x 
        # many trapezoids
        gb_nd[:,1] = gb_nd[:,1]*1000 # DELETE!!!!!!!!!!!!!
        if not onevertex:
        
            for idx, d in enumerate(gb_nd[:-2,0]):
                vertices = []
                if d < extent:
                    vertices.append(mp.Vector3(d,0,surface_offset))      
                    vertices.append(mp.Vector3(d,0,surface_offset+gb_nd[idx,1]))
                    vertices.append(mp.Vector3(gb_nd[idx+1,0], 0,surface_offset+gb_nd[idx+1,1]))
                    vertices.append(mp.Vector3(gb_nd[idx+1,0], 0, surface_offset))
                #if idx < 5:  print(vertices)    
                    geometry.append(mp.Prism(vertices, axis=mp.Vector3(0,1,0), height=0,
                                             material=material))
        else:
            #One vertex
            vertices = []
            vertices.append(mp.Vector3(0,0,surface_offset))
            for idx, d in enumerate(gb_nd[:,0]):
                if d <extent:
                    vertices.append(mp.Vector3(d, 0, surface_offset+gb_nd[idx,1]))
            vertices.append(mp.Vector3(extent, 0,surface_offset))
            geometry.append(mp.Prism(vertices, axis=mp.Vector3(0,1,0), height=0, material=mp.Medium(epsilon=10, D_conductivity=37)))
        
        return geometry


if __name__ == "__main__":
    
    mp_units = MeepUnits(a=1000, resolution=100)
    
    subsRS = [{"i":10.7, 
             "tau1":0.25, 
             "tau2":2.5,
             "n":2,
             },
             {"i":6.5, 
             "tau1":2,
             "tau2":230, 
             "n":2,
             }]
 

    mtlm = {
            "type":"exp",
            "lambda":2000,
            }
    src_configs = {"src_x": 0,
                   "surface_z": 0, #the surface height (z) of the src x location, which can be offset by src_offset.
                   "mtlm": mtlm,
                   "v_rs": 1.5,
                   "sources_per_unit": 250, 
                   "stroke_length": 8,
                   "source_component": None,
                   "channel_base_current": None,
                   }
    firstRS = [{"i":28, 
             "tau1":1.8, 
             "tau2":95,
             "n":2,
                 }]



    src_configs["channel_base_current"] = subsRS
    h1 = RS_Model(src_configs=src_configs.copy(), tower=None, 
                  discontinuity=False, t_ord_of_magnitude=1e-6, a=1000, resolution=250,
                  )
    ax = h1.plot_current(80,0.01, "Subsequent RS", normalize=False, figure="new")

    src_configs["channel_base_current"] = firstRS
    h2 = RS_Model(src_configs=src_configs.copy(), tower=None, 
                  discontinuity=False, t_ord_of_magnitude=1e-6, a=1000, resolution=250,
                  )
    h2.plot_current(80,0.01, "First RS", normalize=False, figure="new")
    plt.show()


