"""
Created on Fri Mar 12, 2021 18:20:00

@author: Hannes Kohlmann
@company: OVE Service GmbH 
@website: https://www.aldis.at
ALDIS - Austrian Lightning Detection Information System
"""

import pandas as pd
import h5py
import numpy as np
import sys, os
import matplotlib.pyplot as plt

def plot(csv_file, same_figure=False):
    
    df = pd.read_csv(csv_file)
    keys = list(df.columns.values)
    time = df['Time'].values
    if "E" in keys[3:][0]:
       y_label="V/m"
    else:
       y_label="A/m"
    if not same_figure:
        fig = plt.figure()
    for i in keys[3:]:
        field_over_time = df[i].values
        plt.plot(time, field_over_time, label=i)                    
        ax = plt.gca()
    plt.legend()
    plt.gca().set_xlabel("Time (μs)")
    plt.gca().set_ylabel("Amplitude ("+y_label+")")
    plt.tight_layout()

if __name__ == "__main__":
    plot("./sim-output/field_output_Ez.csv")
    plot("./sim-output/field_output_Ex.csv")
    plot("./sim-output/field_output_Hy.csv")
    plt.show()
