"""
Created on Fri Mar 12, 2021 18:20:00

@author: Hannes Kohlmann
@company: OVE Service GmbH 
@website: https://www.aldis.at
ALDIS - Austrian Lightning Detection Information System
"""

# *****************************************************************************
# ************************ Import of libraries ********************************
# *****************************************************************************
import sys, os
import datetime
sys.path.append("..")
script_filedir = os.path.dirname(os.path.realpath(__file__))
script_filename = os.path.basename(os.path.realpath(__file__))
output_dir = script_filedir+"/sim-output"

import meep as mp
import pandas as pd
import numpy as np
import math as m
import matplotlib.pyplot as plt
from utility_functions import *

field_component_meep =  {
                        "Ex": [mp.Er, mp.output_efield_r],
                        "Ey": [mp.Ep, mp.output_efield_p],
                        "Ez": [mp.Ez, mp.output_efield_z],
                        "Hx": [mp.Hr, mp.output_hfield_r],
                        "Hy": [mp.Hp, mp.output_hfield_p],
                        "Hz": [mp.Hz, mp.output_hfield_z],
                        }
                        
# *****************************************************************************
# **************** CONFIGURATIONS AND PARAMETERS ******************************
# *****************************************************************************

a = 1000 # MEEP unit size in m
eps0 = 8.854e-12 # ε_0 dielectric constant
c0 = 299792458
i0 = 1 #  kilo Ampere
time_unit = a/c0 * 1e6 # propagation of one MEEP unit distance in microseconds

time = 130 # total simulation time (in MEEP units)
# res=50 --> 1km/50 = 20m; res=30 --> 1km/30 = 33,3m; res=20 --> 1km/20 = 50m; res=25 ==> 1km/25 = 40m,

# res=25, for instance, produces heavily altered waveforms! It is suspected to be related to numerical dispersion.
resolution = 20 # resolution of the unit cell (1km/resolution is one Δx)
courant_factor = 0.67
output_filename = "field_output" # name of csv file containing the field output

size_x = 120 # size of simulation cell in x direction (km)
pml_thickness = 2 # thickness of PML at the edge of the cell
cell_x = size_x + pml_thickness # extends cell by pml thickness at outer end
above_ground = 1/resolution # measure above ground (km) to avoid averaged permittivity effects

# Lightning channel / return stroke model:
stroke_type = "subsequent" # "first", "subsequent", "custom" or "real"
stroke_length = 10 # length of return stroke channel in km
v_rs = 1.5e8 # m/s
with_tower=False
sources_per_yeecell = 1
sources_per_unit = resolution*sources_per_yeecell # nr of sources per MEEP unit. 
source_extent = 1/(sources_per_unit) # this is the distance between two sources for given sources per unit
src_offset = 0 #1/resolution
total_nr_src = int(stroke_length * sources_per_unit) # nr of sources making up the whole channel
src_x = 0 # distance of vertical channel from the origin. 0 distributes current sources at the z-(symmetry) axis
src_component = mp.Ez # Ez is the only practical component!

# ****************  TERRAIN MODEL INITIALITATION, IF NOT FLAT ******************
with_terrain = False # terrain model will be used if True, else FLAT
prism_model = True # terrain model as prism model, else material function (with more severe stair stepping!)
epsilon_averaging = True # epsilon averaging of material leads to smoother corner edges
if with_terrain: # else just flat 
    terrain_filename = "./terrain_data/gaisberg_neudorf_fineRes.csv"
    # TerrainModel is a self-written class related to a special terrain model (analytical or real terrain)
    terrain_model = TerrainModel(kind="file", cell_x=cell_x, resolution=resolution, filename=terrain_filename )

# ***************** MATERIAL PARAMETERS AND TERRAIN MODEL *********************
# Here, the material parameters are initialized
epsr = 10 # Remark: -1e20 (-infinity) corresponds to a mp.perfect_electric_conductor
E_conductivity = 0.001 # Conductivity in S/m
D_conductivity_mp = a/(eps0*epsr*c0)*E_conductivity # convert to dimension-free MEEP D_conductivity
# specifies lossy ground medium
ground_material = mp.Medium(epsilon=epsr, D_conductivity=D_conductivity_mp) 
# specifies air medium (no conductivity: 1e-20, else D_conductivity of ground_material gets ignored in material function)
air_material = mp.Medium(epsilon=1, D_conductivity=1e-20)  

ground_thickness = 1
obj_offset = pml_thickness # the outer layer is reserved for PML, which would otherwise just overwrite the geometry

if not with_terrain: # flat ground
    ground_thickness = 1
    cell_z = stroke_length + 2*pml_thickness + ground_thickness # set up cell extent in z-direction, including PMLs
    geom_center_point = -cell_z/2 + obj_offset + ground_thickness/2
    geom_ground_point = -cell_z/2 + obj_offset # for keeping track of reference height levels
    print("Geom center point:", geom_center_point)
    print("Geom ground point:", geom_ground_point)
    surface_at_origin = geom_ground_point + ground_thickness
else: # for keeping the channel length constant. Otherwise it is cut off earlier in height --> artefacts
    ground_thickness = 0.5 + terrain_model.max # add buffer of 0.5 to the maximum terrain elevation
    cell_z = stroke_length + 2*pml_thickness + ground_thickness # set up cell extent in z-direction, including PMLs
    geom_center_point = -cell_z/2 + obj_offset + ground_thickness/2
    geom_ground_point = -cell_z/2 + obj_offset # for keeping track of reference height levels
    surface_at_origin = geom_ground_point + terrain_model.terrain_fct(src_x) # evaluates terrain height at source location and adds
print("Ground level at origin: "+str(surface_at_origin))

# **** Current source model of lightning channel: PARAMETER SPECIFICATION + MTLM + TOWER MODEL  ************
# 'stroke' is a Python dictionary (dict), initialized as {"keyword": value}: "i" is the current amplitude (kA)
# "tau1","tau2" are the time constants and n further coefficient of the Heidler functions

if stroke_type == "first":
    stroke = [{"i":28, "tau1":1.8, "tau2":95, "n":2}]
elif stroke_type == "subsequent":
    stroke = [{"i":10.7, "tau1":0.25, "tau2":2.5, "n":2},
              {"i":6.5, "tau1":2, "tau2":230, "n":2}]    
elif stroke_type == "custom":
    stroke = [{"i":15, "tau1":1.1, "tau2":1.7, "n":2},
              {"i":13, "tau1":2, "tau2":70, "n":2}]
else:
    print("Stroke type does not exist:", stroke_type)
    raise ValueError

#  'mtlm' is a dictionary holding modified transmission line model parameters:
# "type" can be "exp" (exponential) or "lin" (linear); "lambda" is the decay length of the channel currents
# in meters: in the exponential case [e^(-height/lambda)] and in the linear case [1-height/lambda]
mtlm = {"type": "exp", "lambda": 2000}
# write the source current-related data into a dict:
src_configs = {"src_x": src_x,
               "surface_z": surface_at_origin+src_offset, #the surface height (z) of the src x location, which can be offset by src_offset.
               "mtlm": mtlm,
               "v_rs": v_rs,
               "sources_per_unit": sources_per_unit, 
               "stroke_length": stroke_length,
               "source_component":mp.Ez,
               "channel_base_current": stroke,
               }

if with_tower: # tower is a dict with
    # "H": tower size in km, "rho_t" and "rho_b" are top and ground reflection coeffs and n the number of channel injections
    tower = {"H": 0.100, "rho_t":-0.53, "rho_g":0.7, "n":10}

rs_model = RS_Model(src_configs=src_configs, 
                    tower=tower if with_tower else None, 
                    discontinuity=False, t_ord_of_magnitude=1e-6, a=a, resolution=resolution)

# ***** MATERIAL FUNCTION (if with_terrain and prism model is not activated) *****
if with_terrain:
    if prism_model: # build terrain with prism primitives
        geometry = terrain_model.prism_func(geom_ground_point, cell_x, ground_material)
    else:  # build terrain with material function
        def material_fct(p): # compares Vector3 type MEEP location p (like p.z) to height threshold
                # ground level at distance p.x:
                h_threshold = geom_ground_point + terrain_model.terrain_fct(p.x)
                if (p.z<=h_threshold):
                        return ground_material
                else:
                        return air_material
        geometry = [mp.Block(center=mp.Vector3(0,0,geom_center_point), 
                             size=mp.Vector3(mp.inf,0,ground_thickness),  
                             material=material_fct)]
else: # flat
    geometry = [mp.Block(center=mp.Vector3(0,0,geom_center_point), 
                         size=mp.Vector3(mp.inf,0,ground_thickness),  
                         material=ground_material)]

# boundary conditions: perfectly matched layer
pml_layers = [mp.PML(thickness=pml_thickness, direction=mp.R, side=mp.High), 
              mp.PML(thickness=pml_thickness, direction=mp.Z, side=mp.High),
              mp.PML(thickness=pml_thickness, direction=mp.Z, side=mp.Low),
                 ] 

cell = mp.Vector3(cell_x,0,cell_z) # 2d cylindrical symmetry cell
sources = rs_model.sources()
sim_config = {"cell_size": cell,
              "boundary_layers": pml_layers,
              "sources": sources,
              "resolution": resolution,
              "Courant": courant_factor,
              "dimensions": mp.CYLINDRICAL,
              }

# set epsilon averaging / add material_fct to extra_materials of sim_config
if with_terrain == True:
    if prism_model:     sim_config["eps_averaging"] = True
    else: # material function
        sim_config["extra_materials"] = [material_fct(mp.Vector3())]
        sim_config["eps_averaging"] = False # computationally too expensive

sim_config["geometry"] = geometry

# *******************************************************
# *************** Field output **************************
# *******************************************************
field_readout_components = ["Ez", "Ex", "Hy"]
readout_at = np.r_[50.0:110.0:10.0] # a NumPy array of distances in km, readout every 2m
readout_at_every = 0.05 # MEEP time!
to_csv_at_every = int(time/100) # output of fields every time/100 time step

print("Readout at z=",geom_ground_point+above_ground)
# construct field readout vector:
_readout_vectors = [(d_f_s,0, surface_at_origin+above_ground
                                         +(terrain_model.terrain_fct(d_f_s) if with_terrain==True else 0)) for d_f_s in readout_at]
fields = {} # this is a dict of dicts, where each key (e.g. "Ez", "Hy") is separated
for f in field_readout_components:
    fields[f] = {"Time": [], "MEEP_Time":[]} # both real Time (mu s) and MEEP_time exported
    for r in _readout_vectors:
        d = round(r[0], 3)
        height = round(r[2]-geom_ground_point, 3) # indicates real height (in km)
        key = f+"_("+str(d)+","+str(height)+")"# this is the full field key of the table column
        fields[f][key] = []

def read_out_field(sim):
    """This function outputs the specified fields"""
    cyl_scaling_factor = 4/m.pi
    e_conversion_factor = i0/(a*c0*eps0)
    mp_t = sim.meep_time()
    t_real = mp_t*time_unit # in micro seconds
    for field in field_readout_components:
        fields[field]["MEEP_Time"].append(mp_t)
        fields[field]["Time"].append(t_real)
        for r in _readout_vectors:
            d = round(r[0], 3)
            height = round(r[2]-geom_ground_point, 3)
            key = field+"_("+str(d)+","+str(height)+")"
            if field.find("E") != -1: # scaling of E-field due to unit-freedom (MEEP)
                e_conv = e_conversion_factor
            elif field.find("H"): # no scaling for H-field
                e_conv = 1
            fields[field][key].append(cyl_scaling_factor*resolution*e_conv*np.real(sim.get_field_point(
                                field_component_meep[field][0], # picks the "mp.Ez" for example
                                mp.Vector3(r[0],r[1], r[2]),
                                )))
def write_fields_to_file(sim):
    for field in field_readout_components:
        data = pd.DataFrame.from_dict(fields[field])
        csv_path = output_dir + "/"+output_filename+"_"+field+".csv"
        data.to_csv(csv_path)
# *******************************************************
# *********END OF FIELD OUTPUT **************************
# *******************************************************

# Create arg\_list to be passed to the sim.run(*arg\_list, **keyworded\_args)
arg_list = []
if _readout_vectors != []:
    arg_list.append(mp.at_every(readout_at_every, read_out_field ))
    arg_list.append(mp.at_every(to_csv_at_every, write_fields_to_file))
    
output_h5 = [] # for example output_h5 = ["Ez"]
h5_at_every = 0.5
for field in output_h5:
        arg_list.append(mp.to_appended(field, mp.at_every(h5_at_every, field_component_meep[field][1])))

# Create keyworded\_args (kwargs)
keyworded_args = {
        "until": time,
        }

# instanciate Simulation class by passing keyworded arguments **sim\_config to it
sim = mp.Simulation(**sim_config) 
sim.use_output_directory(script_filedir+"/sim-output") # creates a directory for all outputs
sim.run(*arg_list,**keyworded_args)
write_fields_to_file(sim)
